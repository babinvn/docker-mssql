# Using MS SQL Server docker container

Use a dedicated volume to store the database data files mounted as `/var/docker/mssql`.
Create two subdirectories and grant them to `mssql` user:

```
mkdir -p /var/docker/mssql/system /var/docker/mssql/user
chown -R 10001 /var/docker/mssql/system /var/docker/mssql/user
```

To connect to the database:

```
docker exec -it mssql /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P <password>
```

To configure the database for contained authentication:

```
USE [master];
GO
EXEC sp_configure 'show advanced', 1
GO
RECONFIGURE
GO
EXEC sp_configure 'contained database authentication', 1
GO
RECONFIGURE
GO
```

For example of creating contained databases please check https://bitbucket.org/babinvn/docker-jira-confluence/src/master/mssql/